<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <!-- TODO : moitié %, moitié effectif + survol = active les cases qui se correspondent -->

    
    <xsl:param name="typeMap">
        <xsl:copy-of select="//tei:list[@xml:id = 'typology_commentary']"/>
    </xsl:param>
    
    <xsl:template match="TEI">
        <xsl:variable name="data">
            <xsl:copy-of select="."/>
        </xsl:variable>
        <style>
            /*.reset-styles{
            background-color:inherit !important;
            }*/
            .hide {
                display: none;
            }</style>
        <!--<a data-pos="right" class="btn btn-sm btn-outline-secondary reset-styles">
            Activer/Désactiver le surlignage
        </a>
        <mark>bidule</mark>
        -->
        <div>
            <h4>Mode d'emploi</h4>
            <p>Ces tableaux permettent de connaître le nombre de commentaires de chaque type mais
                aussi d'observer les correspondance entre type de commentaires. Le tableau est
                entièrement cliquable et permet de visualiser le sous-ensemble de commentaires
                correspondant à la sélection.</p>
            <xsl:variable name="resp" as="element()*">
                <resp encoding="all">Tous</resp>
                <xsl:for-each-group select="text/body/div/div/div" group-by="@resp">
                    <resp>
                        <xsl:attribute name="encoding">
                            <xsl:value-of select="substring(@resp, 2)"/>
                        </xsl:attribute>
                        <xsl:value-of select="@resp"/>
                        <!-- TODO: Have a name for each commentator -->
                    </resp>
                </xsl:for-each-group>
            </xsl:variable>

            <div class="form-group row ml-2">
                <label for="commentatorSelect" class="col-form-label mr-1">Filtrer par
                    commentateur</label>
                <select class="col-1 form-control form-control-sm form-control-inline custom-select"
                    id="tableaux-commentatorSelect">
                    <xsl:for-each select="$resp">
                        <option>
                            <xsl:attribute name="data-commentator-select">
                                <xsl:value-of select="@encoding"/>
                            </xsl:attribute>
                            <xsl:if test="@encoding = 'all'">
                                <xsl:attribute name="selected"/>
                            </xsl:if>
                            <xsl:value-of select="."/>
                        </option>
                    </xsl:for-each>
                </select>
            </div>
            <xsl:for-each select="text/front//div[@type = 'libre']">
                <xsl:variable name="liber">
                    <xsl:value-of select="@xml:id"/>
                </xsl:variable>
                <xsl:variable name="countInLiber">
                    <xsl:value-of
                        select="count($data//tei:body//tei:div/p/ref[substring(@target, 3, 1) = substring($liber, 2)])"
                    />
                </xsl:variable>
                <div style="display:inline-block;">
                    <table class="table table-sm table-hover border-0">
                        <thead class="border-0">
                            <tr>
                                <th scope="col" class="border-0 text-center">
                                    <h4>Livre <xsl:value-of select="substring($liber, 2)"/></h4>
                                    <b class="d-block">
                                        <xsl:for-each select="$resp">
                                            <span>
                                                <xsl:attribute name="class">
                                                  <xsl:text>text-center </xsl:text>
                                                  <xsl:if test="not(@encoding = 'all')"
                                                  ><xsl:text>hide</xsl:text></xsl:if>
                                                </xsl:attribute>
                                                <xsl:attribute name="data-commentateur">
                                                  <xsl:value-of select="@encoding"/>
                                                </xsl:attribute>
                                                <xsl:value-of select="."/>
                                            </span>
                                        </xsl:for-each>
                                    </b>
                                    <i>
                                        <xsl:value-of select="$countInLiber"/>
                                        <xsl:text> commentaire</xsl:text>
                                        <xsl:if test="$countInLiber &gt; 1">
                                            <xsl:text>s</xsl:text>
                                        </xsl:if>
                                    </i>
                                </th>
                                <xsl:for-each select="$typeMap//item[not(@xml:id = 'default')]">
                                    <th scope="col">
                                        <xsl:attribute name="style">
                                            <xsl:text>writing-mode: tb;</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="class">
                                            <xsl:text>p-3 m-0 border-0</xsl:text>
                                        </xsl:attribute>
                                        <a>
                                            <xsl:attribute name="data-label">
                                                <xsl:value-of select="@xml:id"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="href">
                                                <xsl:text disable-output-escaping="yes">?page=livre</xsl:text>
                                                <xsl:value-of select="substring($liber, 2)"/>
                                                <xsl:text disable-output-escaping="yes">&amp;filters=</xsl:text>
                                                <xsl:value-of select="@xml:id"/>
                                                <xsl:text disable-output-escaping="yes">&amp;op=and</xsl:text>
                                            </xsl:attribute>
                                            <xsl:attribute name="title">
                                                <xsl:text>Commentaire de type "</xsl:text>
                                                <xsl:value-of select="concat(upper-case(substring(tei:name,1,1)),substring(tei:name,2))"/>
                                                <xsl:text>", encodé en TEI dans l'attribut @ana avec la valeur "</xsl:text>
                                                <xsl:value-of select="@xml:id"/>
                                                <xsl:text>"</xsl:text>
                                            </xsl:attribute>
                                            <xsl:text>⬤ </xsl:text>
                                            <xsl:value-of select="concat(upper-case(substring(tei:name,1,1)),substring(tei:name,2))"/>
                                        </a>
                                    </th>
                                </xsl:for-each>
                            </tr>
                        </thead>
                        <tbody style="border:0;">
                            <xsl:for-each select="$typeMap//item[not(@xml:id = 'default')]">
                                <xsl:variable name="rowVal">
                                    <xsl:value-of select="@xml:id"/>
                                </xsl:variable>
                                <xsl:variable name="rowEnc">
                                    <xsl:value-of select="@xml:id"/>
                                </xsl:variable>
                                <xsl:variable name="rowName">
                                    <xsl:value-of select="concat(upper-case(substring(tei:name,1,1)),substring(tei:name,2))"/>
                                </xsl:variable>
                                <xsl:variable name="rowPos" as="xs:integer">
                                    <xsl:value-of select="position()"/>
                                </xsl:variable>
                                <tr style="border:0;">
                                    <th scope="row" class="text-right pr-3" style="border:0;">
                                        <a>
                                            <xsl:attribute name="data-label">
                                                <xsl:value-of select="@xml:id"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="href">
                                                <xsl:text disable-output-escaping="yes">?page=livre</xsl:text>
                                                <xsl:value-of select="substring($liber, 2)"/>
                                                <xsl:text disable-output-escaping="yes">1&amp;filters=</xsl:text>
                                                <xsl:value-of select="@xml:id"/>
                                                <xsl:text disable-output-escaping="yes">&amp;op=and</xsl:text>
                                            </xsl:attribute>
                                            <xsl:attribute name="title">
                                                <xsl:text>Commentaire de type "</xsl:text>
                                                <xsl:value-of select="concat(upper-case(substring(tei:name,1,1)),substring(tei:name,2))"/>
                                                <xsl:text>", encodé en TEI dans l'attribut @ana avec la valeur "</xsl:text>
                                                <xsl:value-of select="@xml:id"/>
                                                <xsl:text>"</xsl:text>
                                            </xsl:attribute>
                                            <xsl:text>⬤ </xsl:text>
                                            <xsl:value-of select="concat(upper-case(substring(tei:name,1,1)),substring(tei:name,2))"/>
                                            <xsl:comment>
                                    <xsl:text> (</xsl:text>
                                    <xsl:value-of select="count($data//tei:body//tei:div[contains(@ana, $rowEnc)]/p/ref[substring(@target, 3, 1) = substring($liber, 2)])"/>
                                    <xsl:text>)</xsl:text>
                                </xsl:comment>
                                        </a>
                                    </th>
                                    <xsl:for-each select="$typeMap//item[not(@xml:id = 'default')]">
                                        <xsl:variable name="colVal">
                                            <xsl:value-of select="@xml:id"/>
                                        </xsl:variable>
                                        <xsl:variable name="colEnc">
                                            <xsl:value-of select="@xml:id"/>
                                        </xsl:variable>
                                        <xsl:variable name="colName">
                                            <xsl:value-of select="concat(upper-case(substring(tei:name,1,1)),substring(tei:name,2))"/>
                                        </xsl:variable>
                                        <xsl:variable name="colPos" as="xs:integer">
                                            <xsl:value-of select="position()"/>
                                        </xsl:variable>
                                        <xsl:for-each select="$resp">
                                            <xsl:variable name="commentator">
                                                <xsl:value-of select="@encoding"/>
                                            </xsl:variable>
                                            <td>
                                                <xsl:attribute name="style">
                                                  <xsl:text>border:0;</xsl:text>
                                                </xsl:attribute>
                                                <xsl:attribute name="class">
                                                  <xsl:text>text-center </xsl:text>
                                                  <xsl:if test="not(@encoding = 'all')"
                                                  >hide</xsl:if>
                                                </xsl:attribute>
                                                <xsl:attribute name="data-commentateur">
                                                  <xsl:value-of select="$commentator"/>
                                                </xsl:attribute>
                                                <xsl:variable name="count">
                                                  <xsl:choose>
                                                  <xsl:when test="$commentator = 'all'">
                                                  <xsl:value-of
                                                  select="count($data//tei:body//tei:div[contains(@ana, $colEnc) and contains(@ana, $rowEnc)]/p/ref[substring(@target, 3, 1) = substring($liber, 2)])"
                                                  />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:value-of
                                                  select="count($data//tei:body//tei:div[contains(@ana, $colEnc) and contains(@ana, $rowEnc) and substring(@resp, 2) = $commentator]/p/ref[substring(@target, 3, 1) = substring($liber, 2)])"
                                                  />
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                </xsl:variable>
                                                <xsl:choose>
                                                  <xsl:when test="$colPos &lt; $rowPos">
                                                  <xsl:text/>
                                                  </xsl:when>
                                                  <xsl:when test="$colPos = $rowPos">
                                                  <a>
                                                  <xsl:attribute name="class">
                                                  <xsl:text>text-muted</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="href">
                                                  <xsl:text disable-output-escaping="yes">?page=livre</xsl:text>
                                                  <xsl:value-of select="substring($liber, 2)"/>
                                                  <xsl:text disable-output-escaping="yes">&amp;filter_is=</xsl:text>
                                                  <xsl:value-of select="$colVal"/>
                                                  <xsl:if test="not($colVal = $rowVal)">
                                                  <xsl:text disable-output-escaping="yes">,</xsl:text>
                                                  <xsl:value-of select="$rowVal"/>
                                                  </xsl:if>
                                                  <xsl:if test="not($commentator = 'all')">
                                                  <xsl:text disable-output-escaping="yes">,</xsl:text>
                                                  <xsl:value-of select="$commentator"/>
                                                  </xsl:if>
                                                  <xsl:text disable-output-escaping="yes">&amp;op=and&amp;viewall=off</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="title">
                                                  <xsl:choose>
                                                  <xsl:when test="$count = 0">
                                                  <xsl:text>Il n'y a pas de comentaire</xsl:text>
                                                  </xsl:when>
                                                  <xsl:when test="$count = 1">
                                                  <xsl:text>Il y a </xsl:text>
                                                  <xsl:value-of select="$count"/>
                                                  <xsl:text> commentaire</xsl:text>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:text>Il y a </xsl:text>
                                                  <xsl:value-of select="$count"/>
                                                  <xsl:text> commentaires</xsl:text>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                  <xsl:text> ayant au moins l'étiquette </xsl:text>
                                                  <i>
                                                  <xsl:value-of select="$colName"/>
                                                  </i>
                                                  </xsl:attribute>
                                                  <b>
                                                  <xsl:value-of select="$count"/>
                                                  </b>
                                                  </a>
                                                  </xsl:when>
                                                  <xsl:when test="$colPos &gt; $rowPos">
                                                  <a>
                                                  <xsl:attribute name="class">
                                                  <xsl:text>text-muted</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="href">
                                                  <xsl:text disable-output-escaping="yes">?page=livre</xsl:text>
                                                  <xsl:value-of select="substring($liber, 2)"/>
                                                  <xsl:text disable-output-escaping="yes">&amp;filter_is=</xsl:text>
                                                  <xsl:value-of select="$colVal"/>
                                                  <xsl:text disable-output-escaping="yes">,</xsl:text>
                                                  <xsl:value-of select="$rowVal"/>
                                                  <xsl:text disable-output-escaping="yes">&amp;op=and&amp;viewall=off</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="title">
                                                  <xsl:choose>
                                                  <xsl:when test="$count = 0">
                                                  <xsl:text>Il n'y a pas de comentaire</xsl:text>
                                                  </xsl:when>
                                                  <xsl:when test="$count = 1">
                                                  <xsl:text>Il y a </xsl:text>
                                                  <xsl:value-of select="$count"/>
                                                  <xsl:text> commentaire</xsl:text>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:text>Il y a </xsl:text>
                                                  <xsl:value-of select="$count"/>
                                                  <xsl:text> commentaires</xsl:text>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                  <xsl:text> ayant au moins les deux étiquettes </xsl:text>
                                                  <i>
                                                  <xsl:value-of select="$colName"/>
                                                  </i>
                                                  <xsl:text disable-output-escaping="yes"> et </xsl:text>
                                                  <i>
                                                  <xsl:value-of select="$rowName"/>
                                                  </i>
                                                  </xsl:attribute>
                                                  <xsl:value-of select="$count"/>
                                                  </a>
                                                  </xsl:when>
                                                </xsl:choose>
                                            </td>
                                        </xsl:for-each>
                                        <!-- END foreach @resp -->
                                    </xsl:for-each>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                </div>
            </xsl:for-each>
        </div>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"/>
        <script>
            $("select#tableaux-commentatorSelect option").on("click", function() {
                var id = $(this).attr('data-commentator-select');
                $(document.getElementById("tableaux").querySelectorAll("[data-commentateur]")).addClass("hide");
                var matches = document.querySelectorAll("[data-commentateur="+id+"]");
                $(matches).removeClass("hide");
            });
            
            /*$('.reset-styles').click(function() {
                $("mark").toggleClass("reset-styles");
            });*/
        </script>
    </xsl:template>
</xsl:stylesheet>
