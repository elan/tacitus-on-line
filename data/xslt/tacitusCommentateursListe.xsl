<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:template match="/TEI">
        <!-- generate "comentators" and "authors" lists -->
        <div id="legend">
            <h4>Mode d'emploi et légende</h4>
            <p>
                <span class="lead">Cette liste est en cours d'établissement</span>
                 Nous ne sommes pas sûr.e.s de l'identité d'un commentateur au moins (identifié dans
                notre édition TEI par <code>#JUN</code>) : s'agit-il d'<i>Hadrianus Junius</i> ou
                bien de <i>Franciscus Junius</i> ?</p>
            <p>Nous sommes aussi à la recherche d'un référentiel de nom de personne auquel nous pourrions nous référer et faire lien.</p>
        </div>
        <ul class="list-unstyled card-columns list-person">
            <xsl:for-each select="//listPerson[@type = 'commentateurs']/person">
                <xsl:sort data-type="text"/>
                <li>
                    <code>#<xsl:value-of select="@xml:id"/></code>
                    <xsl:text disable-output-escaping="yes">&amp;nbsp;</xsl:text>
                    <xsl:for-each select="persName">
                        <xsl:choose>
                            <xsl:when test="position() = 2">
                                <xsl:text> (</xsl:text>
                            </xsl:when>
                            <xsl:when test="position() > 2">
                                <xsl:text>, </xsl:text>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:value-of select="."/>
                        <xsl:choose>
                            <xsl:when test="last() and position() > 1">
                                <xsl:text>)</xsl:text>
                            </xsl:when>
                        </xsl:choose>
                    </xsl:for-each>
                    <xsl:if test="birth or death">
                        <span class="text-muted">
                            <xsl:text> [</xsl:text>
                            <xsl:value-of select="birth"/> - <xsl:value-of select="death"/>
                            <xsl:text>]</xsl:text>
                        </span>
                    </xsl:if>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>
</xsl:stylesheet>
