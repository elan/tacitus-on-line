<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:param name="typeMap">
        <xsl:copy-of select="//tei:list[@xml:id = 'typology_commentary']"/>
    </xsl:param>

    <xsl:template match="TEI">
        <div>
            <h4>Mode d'emploi</h4>
            <p>
                <span class="lead">Les listes de co-occurrences</span>
                 indiquent le nombre de commentaires en fonction des types de commentaires qui leurs
                sont attribués. Elles sont cliquable et renvoient à une vue filtrée des
                commentaires.<br/>
            </p>
            <script>
                $("select#co-occurrences-commentatorSelect option").on("click", function() {
                var id = $(this).attr('data-commentator-select');
                $(document.getElementById("co-occurrences").querySelectorAll("[data-commentateur]")).addClass("hide");
                var matches = document.querySelectorAll("[data-commentateur="+id+"]");
                $(matches).toggleClass("hide");
                });
            </script>
        </div>
        <xsl:comment><xsl:variable name="resp" as="element()*">
            <resp encoding="all">Tous</resp>
            <xsl:for-each-group select="text/body/div/div/div" group-by="@resp">
                <resp>
                    <xsl:attribute name="encoding">
                        <xsl:value-of select="substring(@resp, 2)"/>
                    </xsl:attribute>
                    <xsl:value-of select="@resp"/>
                    <!-- TODO: Have a name for each commentator -->
                </resp>
            </xsl:for-each-group>
        </xsl:variable>
        <div class="form-group row ml-2">
            <label for="commentatorSelect" class="col-form-label mr-1">Filtrer par
                commentateur</label>
            <select class="col-1 form-control form-control-sm form-control-inline custom-select" id="co-occurrences-commentatorSelect">
                <xsl:for-each select="$resp">
                    <option>
                        <xsl:attribute name="data-commentator-select">
                            <xsl:value-of select="@encoding"/>
                        </xsl:attribute>
                        <xsl:if test="@encoding = 'all'">
                            <xsl:attribute name="selected"/>
                        </xsl:if>
                        <xsl:value-of select="."/>
                    </option>
                </xsl:for-each>
            </select>
        </div></xsl:comment>
        <xsl:apply-templates select="text/front" mode="do"/>
    </xsl:template>

    <xsl:template match="teiHeader"/>
    <xsl:template match="text"/>

    <xsl:template match="text/front" mode="do">
        <xsl:for-each select="//div[@type = 'libre']">
            <xsl:variable name="liber">
                <xsl:value-of select="@xml:id"/>
            </xsl:variable>
            <h4>Livre <xsl:value-of select="substring($liber, 2)"/></h4>
            <xsl:comment>TODO : par commentateur</xsl:comment>
            <xsl:comment>Rem. Le nombre de résultats affiché peut différer en cas d'utilisation d'un type de commentaire qui n'est pas dans la typologie déclarée. En effet, les filtres sélectionnent tous les comentaires "à corriger" alors que dans la vue co-occurence, on affiche une information plus fine (la valeur erronée), or les filtres ne sont pas capables de faire une sélection aussi fine. Ainsi, on renvoi systématiquement tous les erronés (donc plus de commentaires que prévu)
            </xsl:comment>
            <xsl:apply-templates select="../../../body">
                <xsl:with-param name="liber">
                    <xsl:value-of select="$liber"/>
                </xsl:with-param>
            </xsl:apply-templates>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="body">
        <xsl:param name="liber"/>

        <xsl:variable name="anaList" as="node()*">
            <xsl:for-each-group
                select="//div/p/ref[substring(@target, 3, 1) = substring-after($liber, 'n')]/../.."
                group-by="normalize-space(@ana)">
                <anaList>
                    <xsl:attribute name="count">
                        <xsl:value-of select="count(current-group())"/>
                    </xsl:attribute>
                    <xsl:for-each select="tokenize(current-grouping-key(), ' ')">
                        <xsl:sort select="." order="ascending" data-type="text"/>
                        <xsl:choose>
                            <xsl:when test=". != ''">
                                <xsl:value-of select="."/>
                            </xsl:when>
                            <xsl:otherwise>
                                <small>
                                    <small>
                                        <i>non classé</i>
                                    </small>
                                </small>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:text> </xsl:text>
                    </xsl:for-each>
                </anaList>
            </xsl:for-each-group>
        </xsl:variable>

        <xsl:for-each-group select="$anaList" group-by=".">
            <xsl:sort order="descending" select="sum(current-group()/@count)"/>
            <xsl:variable name="thisAnaList" select="."/>
            <xsl:if
                test="(position() &lt;= 12) or (sum(current-group()/@count) > 3 and position() &lt;= 12)">
                <xsl:variable name="filter_is">
                    <xsl:for-each select="tokenize(normalize-space(.), ' ')">
                        <xsl:variable name="thisType">
                            <xsl:choose>
                                <xsl:when test="contains(., '#')">
                                    <xsl:value-of select="substring-after(., '#')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="."/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:choose>
                            <xsl:when test="count($typeMap//*[@xml:id = $thisType]/@xml:id) = 1">
                                <xsl:value-of select="$typeMap//*[@xml:id = $thisType]/@xml:id"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>default</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="not(position() = last())">
                            <xsl:text>,</xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:variable name="filter_isnot">
                    <xsl:for-each select="$typeMap/*[@xml:id = 'typology_commentary']/*/@xml:id">
                        <xsl:if test="not(contains(normalize-space($thisAnaList), .))">
                            <xsl:value-of select="."/>
                            <xsl:text>,</xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:variable name="countACorriger">
                        <xsl:for-each select="tokenize(normalize-space(.), ' ')">
                            <xsl:variable name="thisType">
                                <xsl:value-of select="substring-after(., '#')"/>
                            </xsl:variable>
                            <xsl:if
                                test="count($typeMap/*[@xml:id = 'typology_commentary']/*[@xml:id = $thisType]) = 0">
                                <xsl:text>1</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:if test="string-length($countACorriger) = 0">
                        <xsl:text>default</xsl:text>
                    </xsl:if>
                </xsl:variable>
                <a type="button" class="btn btn-light mr-1 mb-1 border-secondary" role="button">
                    <xsl:attribute name="href">
                        <xsl:text>?page=livre</xsl:text>
                        <xsl:value-of select="substring($liber, 2)"/>
                        <xsl:text>&amp;filter_is=</xsl:text>
                        <xsl:value-of select="$filter_is"/>
                        <xsl:text>&amp;filter_isnot=</xsl:text>
                        <xsl:value-of select="$filter_isnot"/>
                        <xsl:text>&amp;op=and&amp;viewall=off</xsl:text>
                    </xsl:attribute>
                    <span class="badge badge-dark mr-1">
                        <xsl:value-of select="sum(current-group()/@count)"/>
                    </span>
                    <xsl:choose>
                        <xsl:when test=". != ''">
                            <xsl:for-each select="tokenize(normalize-space(.), ' ')">
                                <xsl:variable name="thisType">
                                    <xsl:choose>
                                        <xsl:when test="contains(., '#')">
                                            <xsl:value-of select="substring-after(., '#')"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <span>
                                    <xsl:attribute name="data-label" select="$thisType"/>
                                    <xsl:choose>
                                        <xsl:when test="count($typeMap//*[@xml:id = $thisType]) > 0">
                                            <xsl:value-of
                                                select="concat(upper-case(substring($typeMap//*[@xml:id = $thisType]/name, 1, 1)), substring($typeMap//*[@xml:id = $thisType]/name, 2))"
                                            />
                                        </xsl:when>
                                        <xsl:when test="count($typeMap//*[@xml:id = $thisType]) = 0">
                                            <span class="badge badge-danger mr-1"> !! <xsl:value-of
                                                  select="$thisType"/> !! </span>
                                        </xsl:when>
                                    </xsl:choose>
                                </span>
                                <xsl:if test="not(position() = last())">
                                    <xsl:text> + </xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <i>Non classé</i>
                        </xsl:otherwise>
                    </xsl:choose>
                </a>
            </xsl:if>
            <xsl:if test="position() = last()">
                <a type="button" class="btn btn-light mr-1" role="button" data-toggle="collapse">
                    <xsl:attribute name="data-target">
                        <xsl:text>#seeAll_livre_</xsl:text>
                        <xsl:value-of select="$liber"/>
                    </xsl:attribute>
                    <xsl:text>voir/cacher les suivants</xsl:text>
                </a>
            </xsl:if>
        </xsl:for-each-group>
        <div class="collapse">
            <xsl:attribute name="id">
                <xsl:text>seeAll_livre_</xsl:text>
                <xsl:value-of select="$liber"/>
            </xsl:attribute>
            <!---->
            <xsl:for-each-group select="$anaList" group-by=".">
                <xsl:sort order="descending" select="sum(current-group()/@count)"/>
                <xsl:variable name="thisAnaList" select="."/>
                <xsl:if test="4 > sum(current-group()/@count)">
                    <xsl:variable name="filter_is">
                        <xsl:for-each select="tokenize(normalize-space(.), ' ')">
                            <xsl:variable name="thisType">
                                <xsl:choose>
                                    <xsl:when test="contains(., '#')">
                                        <xsl:value-of select="substring-after(., '#')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="."/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="count($typeMap//*[@xml:id = $thisType]/@xml:id) = 1">
                                    <xsl:value-of select="$typeMap//*[@xml:id = $thisType]/@xml:id"
                                    />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>default</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:if test="not(position() = last())">
                                <xsl:text>,</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:variable>
                    <xsl:variable name="filter_isnot">
                        <xsl:for-each select="$typeMap/*[@xml:id = 'typology_commentary']/*/@xml:id">
                            <xsl:if test="not(contains(normalize-space($thisAnaList), .))">
                                <xsl:value-of select="."/>
                                <xsl:text>,</xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                        <xsl:variable name="countACorriger">
                            <xsl:for-each select="tokenize(normalize-space(.), ' ')">
                                <xsl:variable name="thisType">
                                    <xsl:value-of select="substring-after(., '#')"/>
                                </xsl:variable>
                                <xsl:if
                                    test="count($typeMap/*[@xml:id = 'typology_commentary']/*[@xml:id = $thisType]) = 0">
                                    <xsl:text>1</xsl:text>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <xsl:if test="string-length($countACorriger) = 0">
                            <xsl:text>default</xsl:text>
                        </xsl:if>
                    </xsl:variable>
                    <a type="button" class="btn btn-light mr-1 mb-1 border-secondary" role="button">
                        <xsl:attribute name="href">
                            <xsl:text>?page=livre</xsl:text>
                            <xsl:value-of select="substring($liber, 2)"/>
                            <xsl:text>&amp;filter_is=</xsl:text>
                            <xsl:value-of select="$filter_is"/>
                            <xsl:text>&amp;filter_isnot=</xsl:text>
                            <xsl:value-of select="$filter_isnot"/>
                            <xsl:text>&amp;op=and&amp;viewall=off</xsl:text>
                        </xsl:attribute>
                        <span class="badge badge-dark mr-1">
                            <xsl:value-of select="sum(current-group()/@count)"/>
                        </span>
                        <xsl:for-each select="tokenize(normalize-space(.), ' ')">
                            <xsl:variable name="thisType">
                                <xsl:choose>
                                    <xsl:when test="contains(., '#')">
                                        <xsl:value-of select="substring-after(., '#')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="."/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <span>
                                <xsl:if test="count($typeMap//*[@xml:id = $thisType]) > 0">
                                    <xsl:attribute name="data-label" select="$thisType"/>
                                    <xsl:value-of
                                        select="concat(upper-case(substring($typeMap//*[@xml:id = $thisType]/name, 1, 1)), substring($typeMap//*[@xml:id = $thisType]/name, 2))"
                                    />
                                </xsl:if>
                                <xsl:if test="count($typeMap//*[@xml:id = $thisType]) = 0">
                                    <span class="badge badge-danger mr-1"> !! <xsl:value-of
                                            select="$thisType"/> !! </span>
                                </xsl:if>
                            </span>
                            <xsl:if test="not(position() = last())">
                                <xsl:text> + </xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </a>
                </xsl:if>
            </xsl:for-each-group>
        </div>
    </xsl:template>
</xsl:stylesheet>
