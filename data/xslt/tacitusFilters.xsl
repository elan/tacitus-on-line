<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:include href="tacitusCommentariesTypologie.xsl"/>

    <xsl:template match="TEI">

        <!-- generate "Type de commentaires" filters -->
        <xsl:result-document href="../../views/html/filtersCommentaries.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <xsl:for-each select="//list[@xml:id = 'typology_commentary']/item">
                <button type="button" data-criteria-type="type">
                    <xsl:attribute name="id">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>btn-filter btn</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of
                        select="concat(upper-case(substring(name, 1, 1)), substring(name, 2))"/>
                </button>
            </xsl:for-each>
            <button type="button" data-criteria-type="type" id="default" data-theme="default"
                class="btn-filter btn">À corriger</button>
        </xsl:result-document>

        <!-- generate "Type de citation" filters -->
        <xsl:result-document href="../../views/html/filtersQuotes.html" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <xsl:for-each select="//list[@xml:id = 'typology_quote']/item">
                <button type="button" data-criteria-type="quote">
                    <xsl:attribute name="id">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>btn-filter btn</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="name"/>
                </button>
            </xsl:for-each>
        </xsl:result-document>

        <!-- generate "Commentator" filters -->
        <!-- FOREACH LIBER : one output -->
        <xsl:for-each select="//tei:div[@type = 'libre']/@xml:id">
            <xsl:variable name="curent_liber">
                <xsl:value-of select="."/>
            </xsl:variable>
            <xsl:result-document
                href="../../views/html/liber{substring($curent_liber,2)}_filtersCommentators.html"
                method="xhtml" indent="no" omit-xml-declaration="yes">
                <xsl:for-each select="//listPerson[@type = 'commentateurs']/person">
                    <xsl:variable name="current_id" select="@xml:id"/>
                    <xsl:message exclude-result-prefixes="#all">
                        <xsl:value-of select="$current_id"/>
                        <xsl:text>: </xsl:text>
                        <xsl:value-of select="count(//div[@resp = concat('#', $current_id)])"/>
                        <xsl:text> au total dont </xsl:text>
                        <xsl:value-of
                            select="count(//div[@resp = concat('#', $current_id) and starts-with(./tei:p/tei:ref[1]/@target, '#n1')])"/>
                        <xsl:text> au livre I, </xsl:text>
                        <xsl:value-of
                            select="count(//div[@resp = concat('#', $current_id) and starts-with(./tei:p/tei:ref[1]/@target, '#n2')])"/>
                        <xsl:text> au livre II et </xsl:text>
                        <xsl:value-of
                            select="count(//div[@resp = concat('#', $current_id) and starts-with(./tei:p/tei:ref[1]/@target, '#n3')])"/>
                        <xsl:text> au livre III </xsl:text>
                    </xsl:message>
                    <xsl:if test="exists(//div[starts-with(@xml:id,concat($current_id,'_',substring-after($curent_liber,'n')))])">
                    <xsl:if test="count(//div[@resp = concat('#', $current_id)])">
                        <button type="button" data-criteria-type="author">
                            <xsl:attribute name="id">
                                <xsl:value-of select="@xml:id"/>
                            </xsl:attribute>
                            <xsl:attribute name="class">
                                <xsl:text>btn-filter btn </xsl:text>
                            </xsl:attribute>
                            <xsl:value-of select="persName[1]"/>
                        </button>
                    </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:result-document>
        </xsl:for-each>

    </xsl:template>
</xsl:stylesheet>
