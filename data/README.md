# Files and folders organisation
```bash
../../data/
├── manifest
│   └── tacitus-online-manifest.json
├── tei
│   ├── Belles_Lettres
│   │   └── Annales_de_Tacite.xml
│   ├── Encodage
│   │   ├── Entetes
│   │   │   ├── Tacitus-teiHeader_listeCommentateurs.xml
│   │   │   ├── Tacitus-teiHeader_listeSources.xml
│   │   │   └── Tacitus-teiHeader.xml
│   │   ├── Livre_I
│   │   │   ├── Tacite_Livre_I_Lipse.xml
│   │   │   ├── (...)
│   │   ├── Livre_II
│   │   │   ├── Tacite_Livre_II_Lipse.xml
│   │   │   └── (...)
│   │   └── (...)
│   ├── Tacitus_on_line.xml
└── xslt
```

## TEI sources
* `Tacitus_on_line.xml` is the main file. It contains `<xi:include/>` to include the content of all XML needed.
* _to be completed_

## How to add a new set of commentaries?
* Group new commentaries in a `<div/>` with a `@xml:id` (in a new file or in an existing file). For example:

**File:  data/tei/Encodage:Livre_I/Tacite_Livre_I_Autres.xml**
```xml
<div resp="#ketsiah" xml:id="liberI_ALC">
        <!-- encodage des commentaires ici -->
</div>
```
* Update `Tacitus_on_line.xml` to include this new group wit the right path to the file and xpointer (refering to the `@xml:id`)
```xml
<xi:include href="Encodage/Livre_I/Tacite_Livre_I_Autres.xml" xpointer="liberI_ALC">
    <xi:fallback>
        <p>Commentaires de ALC (Alciat) au Livre I manquant.</p>
    </xi:fallback>
</xi:include>
```

