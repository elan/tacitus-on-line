<?php

$page = (isset($_GET["page"])) ? $_GET["page"] : "index" ;
$twig = loadTwig();

echo $twig->render($page.'.html.twig', ['page' => $page]);

function loadTwig()
{
    require_once 'vendor/autoload.php';
    $loader = new Twig_Loader_Filesystem('views');
    $twig = new Twig_Environment($loader, array());

    return $twig;
}
