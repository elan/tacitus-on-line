var url = {
  removeParameter: function(url, key) {
    if (!url) url = window.location.href;
    var hashParts = url.split('#');
    var regex = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");

    if (hashParts[0].match(regex)) {
      url = hashParts[0].replace(regex, '$1');
      url = url.replace(/([?&])$/, '');
      if (typeof hashParts[1] !== 'undefined' && hashParts[1] !== null)
        url += '#' + hashParts[1];
    }

    return url;
  },

  getParameter: function(key) {
    var results = new RegExp('[\?&]' + key + '=([^&]*)').exec(window.location.href);
    if (results == null) {
      return null;
    } else {
      return results[1] || 0;
    }
  }
}

var clipboard = {
  init: function() {
    var clipb = new Clipboard('#share-filters', {
      text: function() {
        var text = filter.save();
        return text;
      }
    });

    clipb.on('success', function(e) {
      tooltip.set('Lien copié !');
      tooltip.hide();
    });

    clipb.on('error', function(e) {
      tooltip.set('Failed!');
      tooltip.hide();
    });

    $('#share-filters').tooltip({
      trigger: 'click',
      placement: 'bottom'
    });
  }
}

var tooltip = {
  set: function(message) {
    $('#share-filters').tooltip('hide')
      .attr('data-original-title', message)
      .tooltip('show');
  },
  hide: function() {
    setTimeout(function() {
      $('#share-filters').tooltip('hide');
    }, 1000);
  }
}
