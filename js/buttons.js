$(".btn-filter").click(function() {
  var criteria = $(this).attr("id");
  filter.execute(criteria);
});

$("#disable-btns").click(function() {
  filter.reset();
});

$("#operator label").click(function() {
  setTimeout(function() {
    var operator = $('#operator label.active input').val();
    filter.operator = operator;
    filter.apply();
  }, 300);
});
$("#viewAllParagraph").click(function() {
    document.getElementById("viewAllParagraph").setAttribute("status", "on");
    document.getElementById("viewCommentedParagraph").setAttribute("status", "off");
    filter.viewAllParagraph = "on";
    filter.apply();
});
$("#viewCommentedParagraph").click(function() {
    document.getElementById("viewAllParagraph").setAttribute("status", "off");
    document.getElementById("viewCommentedParagraph").setAttribute("status", "on");
    filter.viewAllParagraph = "off";
    filter.apply();
});

var button = {
    enable: function(btn) {
	/* var theme = btn.data("theme");
	btn.removeClass("btn-outline-" + theme);
	btn.addClass("btn-" + theme); */
    },
    
    disable: function(btn) {
	/* var theme = btn.data("theme");
	btn.addClass("btn-outline-" + theme);
	btn.removeClass("btn-" + theme) */
    },
    
    enableAll: function() {
	var btns = $(".btn-filter");
	for (var i = 0; i < btns.length; i++) {
	    this.enable($(btns[i]));
	}
    },
    
    disableAll: function() {
	var btns = $(".btn-filter");
	for (var i = 0; i < btns.length; i++) {
	    this.disable($(btns[i]));
	}
    }
};
