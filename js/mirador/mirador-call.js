var manifest_uri = document.querySelector('#manifest-uri').dataset.uri;

console.log("manifest", manifest_uri);


var mirador = Mirador.viewer({
    "id": "manifest-uri" ,
    "manifests": {
      manifest: {
      }
    },
    "language": 'fr',

    "window" : {
        // defaultSidebarPanelHeight: 201,  // Configure default sidebar height in pixels
        "defaultSidebarPanelWidth": 180, // Configure default sidebar width in pixels   
    },
    "windows": [
      {
        // "allowWindowSideBar": true // menu trois tirets
        "allowClose": false,
        "sideBarOpenByDefault": true, /* chemin de fer */
        "loadedManifest": manifest_uri,
        "canvasIndex": 1, /* beginning page */
        "thumbnailNavigationPosition": 'far-right',
        "allowFullscreen": true, // Configure to show a "fullscreen" button in the WindowTopBar
        "allowMaximize": false,
        "sideBarPanel": 'canvas', // Configure which sidebar is selected by default. Options: info, attribution, canvas, annotations, search  
        "sideBarOpen": true,

      }
    ],
    "workspaceControlPanel": {
      "enabled": false, // Configure if the control panel should be rendered.  
                        // Useful if you want to lock the viewer down to only the configured manifests
    },
    theme: {
    "typography": {
      "body1": {
        "fontSize": "0.9rem",
        "letterSpacing": "0em",
        "lineHeight": "1.3em",
      },
    },
  },

    thumbnailNavigation: {
      "defaultPosition": 'off', // Which position for the thumbnail navigation to be be displayed. Other possible values are "far-bottom" or "far-right"
      "displaySettings": true, // Display the settings for this in WindowTopMenu
      "height": 100, // height of entire ThumbnailNavigation area when position is "far-bottom"
      "width": 70, // width of one canvas (doubled for book view) in ThumbnailNavigation area when position is "far-right"
    },

    

  });


