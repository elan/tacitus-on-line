#!/bin/bash

echo "-------| Génération des pages éditoriales [tacitusHeader.xsl]"
saxonb-xslt -ext:on -xi:on data/tei/Tacitus_on_line.xml data/xslt/tacitusHeader.xsl
echo

echo "-------| Génération de la liste des noms de personnes pour le manuel [tacitus_Manual-AuthorsList.xsl]"
saxonb-xslt -ext:on -xi:on data/tei/Tacitus_on_line.xml data/xslt/tacitus_Manual-AuthorsList.xsl
echo

echo "-------| Génération des vues des commentaires par livre [tacitusOnline.xsl]"
saxonb-xslt -ext:on data/tei/Belles_Lettres/Annales_de_Tacite.xml data/xslt/tacitusOnline.xsl
echo



# tacitus_quoteAuthors.xsl


# tacitusCo-occurences.xsl
# tacitusCommentariesTypologie.xsl
# tacitusCommentateursFrise.xsl
# tacitusCommentateursListe.xsl
# tacitusFilters.xsl
# tacitusFrise.xsl
# tacitusInventaire.xsl
# tacitusTable.xsl

