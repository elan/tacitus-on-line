def create_canvases(image_dict, manifest, entities_prefix):
    """ 
    Parse le dictionnaire des images extrait de tei:facsimile et construit la 
    liste des Canvas du manifest IIIF. Chaque Canvas est un dictionnaire 
    (voir https://iiif.io/api/presentation/3.0/#53-canvas pour ses propriétés)
    """

    pageNum = 0

    for image_id in image_dict:

        pageNum += 1

        canvas = {}
        canvas["id"] = entities_prefix + "canvas/" + image_id
        canvas["type"] = "Canvas"

        canvasLabel = {}
        canvasLabel["none"] = [image_id]
        canvas["label"] = canvasLabel

        canvas["width"] = int(image_dict[image_id]["width"])
        canvas["height"] = int(image_dict[image_id]["height"])

        canvasItems = {}
        canvasItems["id"] = entities_prefix + "page/" + image_id
        canvasItems["type"] = "AnnotationPage"

        canvasItemsItems = {}
        canvasItemsItems["id"] = entities_prefix + \
            "annotation/image/" + image_id
        canvasItemsItems["type"] = "Annotation"
        canvasItemsItems["motivation"] = "painting"
        canvasItemsItems["target"] = canvasItems["id"]

        body = {}
        body["id"] = image_dict[image_id]["uri"] + "/full/full/0/default.jpg"
        body["type"] = "dctypes:Image"
        body["format"] = "image/jpeg"
        body["width"] = int(image_dict[image_id]["width"])
        body["height"] = int(image_dict[image_id]["height"])

        service = {}
        service["id"] = image_dict[image_id]["uri"]
        service["type"] = "ImageService3"
        service["profile"] = "level2"

        body["service"] = [service]
        canvasItemsItems["body"] = body
        canvasItems["items"] = [canvasItemsItems]
        canvas["items"] = [canvasItems]

        if "annotations" in image_dict[image_id]:
            create_canvas_annotations(
                canvas, image_dict, image_id, entities_prefix)

        manifest["items"].append(canvas)


def create_canvas_annotations(canvas, image_dict, image_id, entities_prefix):
    """
    construit l'objet "annotationsPage" et son contenu pour le Canvas d'une
    image annotée par des tei:zones  (voir https://iiif.io/api/presentation/3.0/#55-annotation-page  et  #56-annotation)
    """

    # it must be a list even though most of the time it will only contain 1 objet.
    canvas['annotations'] = []
    # individual annotations are inside an annotationPage attached to the Canvas : so we begin by creating it
    annotPage = {}
    annotPage['id'] = entities_prefix + \
        "canvas/" + image_id + "/annotationPage"
    annotPage['type'] = "AnnotationPage"
    annotPage['items'] = []

    # tag_nb = 0 # incremental number to add to the undereferencable annotation URI
    # annotation zones on the surface become 'individual'annotations on the annotationPage
    for zone in image_dict[image_id]["annotations"]:
        # tag_nb += 1
        tag = {}
        tag['id'] = entities_prefix + "/canvas/" + \
            image_id + "/annotationPage/tag/" + zone["value"]
        tag['type'] = "Annotation"
        tag["motivation"] = "tagging"
        tag['target'] = canvas["id"] + "#" + zone['coordinates']
        tag["body"] = {}
        tag["body"]["type"] = "TextualBody"
        tag["body"]["value"] = zone['value']
        tag["body"]["language"] = "fr"
        tag["body"]["format"] = "text/plain"

        annotPage['items'].append(tag)

    canvas['annotations'].append(annotPage)
