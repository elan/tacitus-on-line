def parse_facsimile(facsimile_items, image_dict):
    """ Parcourt récursivement l'élément tei:facsimile et ses éventuels tei:surfaceGrp
    et appelle parse_surface() pour ajouter au dictionnaire image_dict les 
    propriétés des images contenus dans chaque élément tei:surface. 
    tei:surface = list() ou value unique (si une seule par surfaceGrp) : effet de xmltodict 
    tei:surface/@xml:id = clef du dictionnaire
    tei:surfaceGrp/@type et @n : segment du label des images, passé à parse_surface()
    """

    if "@type" in facsimile_items:
        print(facsimile_items["@type"])
        groupeType = facsimile_items["@type"]
    else:
        groupeType = ""

    if "@n" in facsimile_items:
        groupNb = facsimile_items["@n"]
    else:
        groupNb = ""

    groupName = str(groupeType + groupNb)
    # print("groupName: ", groupName)

    parse_surface(facsimile_items, groupName, image_dict)

    if "surfaceGrp" in facsimile_items:
        if isinstance(facsimile_items["surfaceGrp"], list):
            # print("Recursing for ", len(facsimile_items["surfaceGrp"]), " surfaceGrp")
            for surfaceGrp in facsimile_items["surfaceGrp"]:
                parse_facsimile(surfaceGrp, image_dict)

        else:  # there's a group / subgroup on its own, with no siblings
            # print("Recursing for 1 surfaceGrp")
            parse_facsimile(facsimile_items["surfaceGrp"], image_dict)


def parse_surface(surfaceGrp, groupName, image_dict):
    """ Parse un élément tei:surface et ajoute ses propriétés au dictionnaire des images.   
    tei:surface = list() ou value unique : effet de xmltodict
    parse_annotated_zone() : parse les éventuelles tei:zone annotées. 

    clef                            = tei:surface/@xml:id 
    URI de l'API Image de Nakala    = tei:surface/graphic/@url 
    img width                       = tei:surface/graphic/@width 
    img height                      = tei:surface/graphic/@height
    coin_surface_label() :          génère le label de l'image. 
   """

    if "surface" in surfaceGrp:

        # Case 1 : Multiple surfaces in a Group
        if isinstance(surfaceGrp["surface"], list):
            # print("Processes ", len(surfaceGrp["surface"]), " surfaces")
            for surface in surfaceGrp["surface"]:
                create_surface_object(surface, groupName, image_dict)
                parse_annotated_zone(surface, image_dict)

        # Case 2 : A single surface in a Group (unlikely but u gotta stay up for anything !)
        else:
            surface = surfaceGrp["surface"]
            create_surface_object(surface, groupName, image_dict)
            parse_annotated_zone(surface, image_dict)


def coin_surface_label(surface, groupName):
    """  Genère le label d'une image pour le manifest IIIF) : 
        concat optionnel( tei:surfaceGrp/@type + @n) + tei:surface/tei:fw 
    """

    if "#text" in surface["fw"]:
        surface_label = str(groupName + "-" + surface["fw"]["#text"])
    else:
        surface_label = "none"
    return surface_label


def parse_annotated_zone(surface, image_dict):
    """ Parse les éventuelles tei:surface/tei:zone annotées, 
    récupère @value (ici slmt tei:zone/@xml:id)
    récupère les coordonnées sur l'image en les formattant conformément aux specs iiif : xywh=
    l'ajoute à l'image (tei:surface) porteuse dans le dictionnaire image_dict
    """
    if "zone" in surface:
        image_dict[surface["@xml:id"]]["annotations"] = []

        # Multiple annotated zones
        if isinstance(surface["zone"], list):
            for zone in surface["zone"]:
                create_annotation_object(surface, zone, image_dict)

        # Single annotated zone
        else:
            zone = surface["zone"]
            create_annotation_object(surface, zone, image_dict)


def create_surface_object(surface, groupName, image_dict):
    image_dict[surface["@xml:id"]] = {
                    "uri": surface["graphic"]["@url"],
                    "width": surface["graphic"]["@width"][:-2], # [:-2] removes the px ...
                    "height": surface["graphic"]["@height"][:-2],
                    "label": coin_surface_label(surface, groupName)
                }

def create_annotation_object(surface, zone, image_dict):
    annotation = {
                    "coordinates": "xywh=" + zone['@ulx'] + "," + zone['@uly'] + "," + str(int(zone['@lrx'])-int(zone['@ulx'])) + "," + str(int(zone['@lry'])-int(zone['@uly'])),
                    "value": zone["@xml:id"]
                }
    image_dict[surface["@xml:id"]]["annotations"].append(annotation)