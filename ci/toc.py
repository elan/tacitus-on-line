def extract_toc(xml_toc, toc_out, cnt, parent_cnt):
    """
    construit l'objet "annotationsPage" et son contenu pour le Canvas d'une
    image annotée par des tei:zones  (voir https://iiif.io/api/presentation/3.0/#55-annotation-page  et  #56-annotation)
    """
    for msItem in xml_toc:
        cnt += 1

        if parent_cnt == "":
            actual_cnt = str(cnt)
        else:
            actual_cnt = parent_cnt + "-" + str(cnt)

        range_id = "http://localhost/tacitus-on-line/data/manifest/range/" + actual_cnt
        iiifRange = {
            "id": range_id,
            "type": "Range",
            "label": {'fr': [msItem["title"]]},
            "items": []
        }
        #  print(msItem["title"])

        if ("locus" in msItem):
            pagelist = msItem["locus"]
            if isinstance(pagelist, list):
                #  print("\t", "LIST",loc["@facs"][1:] )
                for page in pagelist:
                    append_canvas_to_range(iiifRange, page)
            else:
                #  print("\t", "SINGLE",loc["@facs"][1:] )
                append_canvas_to_range(iiifRange, pagelist)

        if "msItem" in msItem:
            sub_msItem = msItem["msItem"]

            if isinstance(sub_msItem, list):
                #  print("\t","\t", "list")
                extract_toc(sub_msItem, iiifRange["items"], 0, actual_cnt)

            elif isinstance(sub_msItem, dict):
                #  print("\t","\t", "\t","\t", list(sub_msItem.keys()))
                extract_toc(sub_msItem, iiifRange["items"], 0, actual_cnt)

        toc_out.append(iiifRange)

    return toc_out


def append_canvas_to_range(iiifRange, page):
    canvas_id = "http://localhost/tacitus-on-line/data/manifest/canvas/" + \
        page["@facs"][1:]
    iiifItem = {
        "id": canvas_id,
        "type": "Canvas"
    }
    iiifRange["items"].append(iiifItem)