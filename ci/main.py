import xmltodict
import json

import tei
import canvas
import toc


def main():
    # Reading TEI Input
    input_xml = "../data/tei/Encodage/Entetes/Tacitus-teiHeader.xml"
    with open(input_xml, "r") as xml_file_obj:
        # converting xml data to dictionary
        xml_source = xmltodict.parse(xml_file_obj.read(), dict_constructor=dict)
        xml_file_obj.close()

    # EXEC
    image_dict = {}
    facsimile = xml_source['TEI']['facsimile']
    tei.parse_facsimile(facsimile, image_dict)

    # print(image_dict)

    # MANIFEST CREATION

    # config manifest
    manifest_uri = "http://localhost/tacitus-on-line/data/manifest/tacitus-online-manifest.json"
    document_rights = "http://creativecommons.org/licenses/by-sa/4.0"
    document_title = "Taciti et C. Velleii Paterculi scripta quae exstant; recognita, emaculata. Additique commentarii copiosissimi et notae non antea editae"

    # init manifest
    manifest = {
        "@context": "http://iiif.io/api/presentation/3/context.json",
        "type": "Manifest",
        "viewingDirection": "left-to-right",
        "behavior": "paged",
        "id": manifest_uri,
        "label": {"none": [document_title]},
        "rights": document_rights,
        "items": []
    }

    entities_prefix = "http://localhost/tacitus-on-line/data/manifest/"

    canvas.create_canvases(image_dict, manifest, entities_prefix)

    print(len(manifest["items"]))
    print(manifest)

    xml_toc = xml_source["TEI"]["teiHeader"]["fileDesc"]["sourceDesc"]["msDesc"]["msContents"]["msItem"]
    manifest["structures"] = toc.extract_toc(xml_toc, [], 0, "")
    # dump manifest as JSON
    with open("tacitus-online-manifest.json", 'w') as f:
        json.dump(manifest, f)  # f, indent=4
        f.close()

if __name__ == "__main__":
    main()
